package Model;

import java.util.*;

import Model.FileContent;

public class FileList {
	private List<FileContent> files;

	public FileList() {
		files = new ArrayList<FileContent>();
	}

	public List<FileContent> getFiles() {
		return files;
	}

	public void setFiles(List<FileContent> files) {
		this.files = files;
	}

}
