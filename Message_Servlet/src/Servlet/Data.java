package Servlet;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

	public static String GetCurrentTime() {
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
	}
}
