package Servlet;

import java.io.IOException;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user=new User(request.getParameter("username"),request.getParameter("password"));
		if (IsAccount(user.getUsername(), user.getPassword())) {
			HttpSession session = request.getSession();
			session.setAttribute("username", user.getUsername());
			response.sendRedirect("index.jsp");
		} else {
			request.setAttribute("errorInput", "");
			request.getRequestDispatcher("Login.jsp").forward(request, response);
		}
	}

	private boolean IsAccount(String username, String password) {
		Database db = new Database("user");
		ResultSet rs = db.Select(
				String.format("SELECT * from account where Username=\"%s\" and Password=\"%s\"", username, password));
		try {
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
