package Servlet;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Model.FileContent;
import Model.FileList;

@WebServlet("/DeleteMessageServlet")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Database db = new Database("messageboard");
		String id = request.getParameter("Id");
		ResultSet rs = db.Select(String.format("select * from message where id=%s", id));
		try {
			if (rs.next()) {
				Gson gson = new Gson();
				FileList fileList = gson.fromJson(rs.getString("File"), FileList.class);
				for (FileContent fileContent : fileList.getFiles()) {
					new File(fileContent.getFilePath()).delete();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.Execute(String.format("delete from message where Id=%s and Username=\"%s\"", id,
				request.getSession().getAttribute("username")));
		response.sendRedirect("MessageBoard.jsp");
	}

}
