package Servlet;

import java.io.File;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;

import Model.*;

@WebServlet("/AddMessageServlet")
public class AddMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String fileDir = "D:/Desktop/TestFileDir/";
		String fileName = "";
		String content = "";
		String savePath = "";
		FileList fileList = new FileList();
		Gson gson = new Gson();
		try {
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
					switch (item.getFieldName()) {
					case "Content":
						content = item.getString();
						break;
					}
				} else {
					String filePath = item.getName();
					if (!filePath.isEmpty()) {
						fileName = new File(filePath).getName();
						savePath = fileDir + Data.GetCurrentTime() + fileName;
						File file = new File(savePath);
						item.write(file);
						FileContent fileContent = new FileContent(fileName, savePath);
						fileList.getFiles().add(fileContent);
					}
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String fileJson = gson.toJson(fileList).replaceAll("\"", "\\\\\"");
		Database db = new Database("messageBoard");
		db.Execute(String.format("insert into message(Username,Content,File) values(\"%s\",\"%s\",\"%s\")",
				request.getSession().getAttribute("username"), content, fileJson));
		response.sendRedirect("MessageBoard.jsp");

	}
}
