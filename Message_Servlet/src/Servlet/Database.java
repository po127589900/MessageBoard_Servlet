package Servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

	Connection con = null;
	Statement stat = null;
	String databaseName = "";

	public Database(String databaseName) {
		this.databaseName = databaseName;
		Connect();
	}

	private void Connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=GMT";
		String username = "aa";
		String password = "123";
		try {
			con = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet Select(String sql) {
		ResultSet rs = null;
		try {
			stat = con.createStatement();
			rs = stat.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}	
	
	public boolean Execute(String sql) {
		try {
			stat = con.createStatement();
			return stat.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
