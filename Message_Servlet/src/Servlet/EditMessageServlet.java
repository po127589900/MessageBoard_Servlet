package Servlet;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;

import Model.*;

import java.sql.*;
import java.util.List;

@WebServlet("/EditMessageServlet")
public class EditMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Database db = new Database("messageboard");
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String fileDir = "D:/Desktop/TestFileDir/";
		String id = "";
		String fileName = "";
		String content = "";
		String username = (String) request.getSession().getAttribute("username");
		String savePath = "";
		FileList fileList = new FileList();
		Gson gson = new Gson();
		try {
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
					switch (item.getFieldName()) {
					case "Id":
						id = item.getString();
						break;
					case "Content":
						content = item.getString();
						break;
					}
				} else {
					String filePath = item.getName();
					if (!filePath.isEmpty()) {
						fileName = new File(filePath).getName();
						savePath = fileDir + Data.GetCurrentTime() + fileName;
						File file = new File(savePath);
						item.write(file);
						fileList.getFiles().add(new FileContent(fileName, savePath));
					}
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!username.isEmpty()) {
			if (fileList.getFiles().isEmpty()) {
				db.Execute(String.format("update message set Content=\"%s\" where Id=%s and Username=\"%s\"", content,
						id, username));
			} else {
				ResultSet rs = db.Select(String.format("select * from message where id=%s", id));
				try {
					if (rs.next()) {
						FileList deleteFileList = gson.fromJson(rs.getString("File"), FileList.class);
						for (FileContent fileContent : deleteFileList.getFiles()) {
							new File(fileContent.getFilePath()).delete();
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				db.Execute(
						String.format("update message set Content=\"%s\",File=\"%s\" where Id=%s and Username=\"%s\"",
								content, gson.toJson(fileList).replaceAll("\"", "\\\\\""), id, username));
			}
		}
		response.sendRedirect("MessageBoard.jsp");
	}

}
