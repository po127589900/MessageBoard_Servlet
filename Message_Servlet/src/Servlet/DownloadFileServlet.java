package Servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Model.*;

@WebServlet("/DownloadFileServlet")
public class DownloadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("Id");
		String fileName = request.getParameter("FileName");
		Database db = new Database("messageboard");
		ResultSet rs = db.Select(String.format("select * from message where id=%s", id));
		try {
			while (rs.next()) {
				Gson gson = new Gson();
				FileList fileList = gson.fromJson(rs.getString("File"), FileList.class);
				for (FileContent fileContent : fileList.getFiles()) {
					if (fileContent.getFileName().equals(fileName)) {
						String filePath = fileContent.getFilePath();
						response.setContentType("APPLICATION/OCTET-STREAM");
						response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
						FileInputStream fileInputStream = new FileInputStream(filePath);

						PrintWriter out = response.getWriter();
						int i;
						while ((i = fileInputStream.read()) != -1) {
							out.write(i);
						}
						fileInputStream.close();
						out.close();
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
